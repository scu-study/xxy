package day4;



public class Stack {
    public static final int INIT_SIZE = 16;
    private int[] arr;


    public Stack(int size) {
        arr = new int[size];
    }

    //输入初始大小
    public Stack() {
        arr = new int[INIT_SIZE];
    }
    //默认大小为16
    public void push(int number){
        int i = 0;
        while (i < arr.length) {
            if (arr[i] == 0) {
                arr[i] = number;
                break;
            }
            i++;
            }
        if (i == arr.length){
            int [] newArr = new int [arr.length*2];
            arr = newArr;
        }

        //满的时候即扩容
    }
    //入栈
    public int pop() {
        int i = 0;
        arr[i] = 0;
        while (i < arr.length - 1) {
            if (arr[i] == 0) {
                break;
            }
            arr[i] = arr[i + 1];
            i++;
        }
        return arr[i-1];
    }
    //出栈
    public int peek(){
        return arr[0];
    }
    //获取栈顶，但不出栈
    public void narrow() {
        int i = 0;
        while (i <= arr.length) {
            if (arr[i] == 0) {
                break;
            }
            i++;
        }
        if (i <= 0.4 * arr.length) {
            int[] newArr = new int[arr.length / 2];
            arr = newArr;
        }
    }
    //缩容
}
