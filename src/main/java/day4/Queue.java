package day4;

public class Queue {
    public static final int INIT_SIZE = 16;
    private int[] arr;


    public Queue(int size) {
        arr = new int[size];
    }

    //输入初始大小
    public Queue() {
        arr = new int[INIT_SIZE];
    }
    //默认大小为16
    public void offer(int number){
        int i = 0;
        while (i < arr.length) {
            if (arr[i] == 0) {
                arr[i] = number;
                break;
            }
            i++;
        }
        if (i == arr.length){
            int [] newArr = new int [arr.length*2];
            arr = newArr;
        }

        //满的时候即扩容
    }
    //入队
    public int poll(){
        int i = 0;
        int c = arr[0];
        while (i < arr.length - 1) {
            arr[i] = arr[i+1];
            i++;
        }
        return c;
    }
    //出队
    public int peek(){
        return arr[0];
    }
    //获取队首，但不出队
    public void narrow() {
        int i = 0;
        while (i <= arr.length) {
            if (arr[i] == 0) {
                break;
            }
            i++;
        }
        if (i <= 0.4 * arr.length) {
            int[] newArr = new int[arr.length / 2];
            arr = newArr;
        }
    }
    //缩容
}
