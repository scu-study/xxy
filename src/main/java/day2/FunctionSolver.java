package day2;

import java.util.Scanner;

public class FunctionSolver {
    public static void main(String[] args) {
        System.out.print("Pls enter the a,b,c of function ax^2+bx+c=0:");
        Scanner input = new Scanner(System.in);
        double a = input.nextDouble();
        double b = input.nextDouble();
        double c = input.nextDouble();
        double delta = b * b- 4 * a * c;
        if (delta > 0) {
            double r1 = (-b + Math.pow(delta,0.5))/(2*a);
            double r2 = (-b - Math.pow(delta,0.5))/(2*a);
            System.out.println("The equation has two roots" + " " + r1 + " " + r2);}
        else if (delta == 0)    {
            double r1 = (-b)/(2*a);
            System.out.println("The equation has one roots"  + " " + r1 );}
        else {
            System.out.println("The equation has no root" );
        }
        }
    }
