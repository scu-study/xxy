package day2;

import java.util.Scanner;

public class InsideOrNot {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a point with two coordinates: ");
        double x = input.nextDouble();
        double y = input.nextDouble();
        if(x <= 5 & y <= 3){
            System.out.print("Inside");
        }
        else{
            System.out.print("Outside");
        }
    }
}
