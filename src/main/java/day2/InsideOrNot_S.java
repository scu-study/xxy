package day2;

import java.util.Scanner;

public class InsideOrNot_S {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("请输入x1,y1,h1,w1,x2,y2,h2,w2:");
        double x1 = input.nextDouble();
        double y1 = input.nextDouble();
        double h1 = input.nextDouble();
        double w1 = input.nextDouble();
        double x2 = input.nextDouble();
        double y2 = input.nextDouble();
        double h2 = input.nextDouble();
        double w2 = input.nextDouble();
        double c;

        if (x1 > x2) {
            c = x1 - x2;
            x2 = x1 + c;
        }
        if (y1 > y2) {
            c = y1 - y2;
            y2 = y2 + c;
        }
        double x2_x1 = x2 - x1;
        double y2_y1 = y2 - y1;
        if (x2_x1 + w2 / 2 <= w1 / 2) {//如果右边框在内
            if (y2_y1 + h2 / 2 <= h1 / 2) {//如果上边框在内
                System.out.printf("在内");
            } else if (y2_y1 + h2 / 2 > h1 / 2) {//如果上边框在外
                if (y2_y1 - h2 / 2 <= h1 / 2) {//如果下边框在内
                    System.out.printf("重叠");
                }
                else{
                    System.out.printf("在外");
                }//如果下边框在外
            }

        }
        else {//如果右边框在外
            if (y2_y1 + h2 / 2 >= h1 / 2) {//如果上边框在外
                if(y2_y1 - h2 / 2 >= h1 / 2){//如果下边框在外
                    System.out.printf("在外");
                }
                else {//如果下边框在内
                    System.out.printf("重叠");
                }
            }
            else{
                System.out.printf("重叠");
            }

            }

        }
    }

